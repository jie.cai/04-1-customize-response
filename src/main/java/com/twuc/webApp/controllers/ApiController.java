package com.twuc.webApp.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiController {

    // # 2.1

    @GetMapping("/no-return-value")
    public void getNoReturnValue() {
        // nothing
    }

    // # 2.2

    @GetMapping("/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void getNoReturnValueWithAnnotation() {
        // nothing
    }

    // # 2.3

    @GetMapping("/messages/{message}")
    public String getMessageFromPathVariable(
            @PathVariable String message
    ) {
        return message;
    }

    // # 2.4
    // TODO

    // # 2.5
    @GetMapping("/messages/{message}/202")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public String getMessageFromPathVariableWith202(
            @PathVariable String message
    ) {
        return message;
    }

    // # 2.6
    @GetMapping("/message-entities/{message}")
    public ResponseEntity<String> getMessageUseRequestEntity(
            @PathVariable String message
    ) {
        return ResponseEntity
                .ok()
                .header("X-Auth", "me")
                .body(message);
    }
}
