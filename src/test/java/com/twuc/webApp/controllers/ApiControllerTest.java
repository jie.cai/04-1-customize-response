package com.twuc.webApp.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ApiControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void should_get_status_by_void() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect(status().is(200));
    }

    @Test
    void should_get_status_with_annotation_by_void() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect(status().is(204));
    }

    @Test
    void should_get_message_from_path_variable() throws Exception {
        mockMvc.perform(get("/api/messages/hello_world"))
                .andExpect(status().is(200))
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
                .andExpect(content().string("hello_world"));
    }

    @Test
    void should_get_message_from_path_variable_with_202() throws Exception {
        mockMvc.perform(
                get("/api/messages/hello_world/202")
        )
                .andExpect(status().is(202));
    }

    @Test
    void should_get_message_by_response_entity() throws Exception {
        mockMvc.perform(
                get("/api/message-entities/hello_world")
        )
                .andExpect(status().isOk())
                .andExpect(header().string("X-Auth", "me"))
                .andExpect(content().string("hello_world"));
    }
}
